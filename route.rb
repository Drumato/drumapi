require 'json'
require 'sinatra'
require 'time'
require 'csv'
require 'net/http'
require_relative 'weather.rb'

get '/' do
  erb :info
end

get '/info' do 
  info = {
    'Title'=>'Welcome to the API of Drumato',
    'Blog'=>'https://drumato.hatenablog.com/',
    'GitHub'=>'https://github.com/Drumato',
    'GitLab'=>'https://gitlab.com/Drumato',
    'Twitter'=>'https://twitter.com/drumato',
    'Kaggle'=>'https://www.kaggle.com/vicsomed3904'
  }
  info.to_json
end

get '/hotness' do
  articles = {
    'Csv'=>'https://drumato.hatenablog.com/entry/2018/09/30/123624',
    'Xonsh'=>'https://drumato.hatenablog.com/entry/2018/11/03/000000',
    'Git'=>'https://drumato.hatenablog.com/entry/2018/11/17/112825',
    'Guidance'=>'https://drumato.hatenablog.com/entry/2018/11/18/215843',
    'Weather'=>'https://drumato.hatenablog.com/entry/2018/11/27/130534'
  }

  articles.to_json
end

get '/appendix' do
  table = CSV.read('./books.csv')
  table.sort!{|a,b| a[3] <=> b[3]}
  books = []
  table.each do |record|
    books.append(record[0])
  end

  reporter = WeatherReport.new('http://weather.livedoor.com/forecast/webservice/json/v1?city=130010')
  report = reporter.ret_weather
  appendix = {
    'Now'=>"#{Time.now}",
    'Books'=>books,
    'weather'=>{
                'title'=>"#{report['title']}",
                'today'=>"#{report['forecasts'][0]['date']}:#{report['forecasts'][0]['telop']}",
                'tomorrow'=>"#{report['forecasts'][1]['date']}:#{report['forecasts'][1]['telop']}"
              }
  }

  appendix.to_json
end
