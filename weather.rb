require 'json'
require 'net/http'

class WeatherReport
  SUMMARY = %w(date telop)

  def initialize(url)
    @weather = send_request(url)
  end

  def send_request(path)
    response = Net::HTTP.get(URI.parse(path))
    weather = JSON.parse(response)
    return weather
  end

  def ret_weather()
    return @weather
  end

end


